(setq searchResults (list ))
(setq namesOfBlockDefinitionsThatContainASearchResult (list ))
(vlax-for blockDefinition 
	(vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object)))
	; (princ "now looking at blockDefinition ")(princ (vla-get-Name blockDefinition))(princ "\n")
	(vlax-for entity blockDefinition
		(if 
			(= (vla-get-Layer entity) "itg-logo" )
			(progn
				(setq searchResults
					(append
						searchResults
						(list entity)
					)
				)
				
				
				
				(princ "found a ")(princ (vla-get-ObjectName entity))(princ " in ")(princ (vla-get-Name blockDefinition))(princ ".")(princ "\n")
				
				(if 
					(not
						(member 
							(vla-get-Name blockDefinition)
							namesOfBlockDefinitionsThatContainASearchResult
						)
					)
					(progn
						(setq namesOfBlockDefinitionsThatContainASearchResult
							(append namesOfBlockDefinitionsThatContainASearchResult (list (vla-get-Name blockDefinition)))
						)
					)
				)
			)
		)
	)
)
(setq blockDefinitionsThatContainASearchResult 
	(mapcar 
		'(lambda (x) (vla-Item  (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object))) x))
		namesOfBlockDefinitionsThatContainASearchResult
	)
)
; (princ "blockDefinitionsThatContainASearchResult: ")(princ blockDefinitionsThatContainASearchResult)(princ "\n")
; (princ "searchResults: ")(princ searchResults)(princ "\n")

(foreach blockDefinitionThatContainsASearchResult blockDefinitionsThatContainASearchResult
	(princ "now searching for references to the block definition ")(princ (vla-get-Name blockDefinitionThatContainsASearchResult))(princ "\n")
	(setq numberOfHits 0)
	(vlax-for blockDefinition 
		(vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object)))
		; (princ "now looking at blockDefinition ")(princ (vla-get-Name blockDefinition))(princ "\n")
		(vlax-for entity blockDefinition
			(if 
				(and
					(= (vla-get-ObjectName entity) "AcDbBlockReference" )
					(= (vla-get-EffectiveName entity) (vla-get-Name blockDefinitionThatContainsASearchResult))
				)
				(progn
					(princ "\tThe block definition ")(princ (vla-get-Name blockDefinitionThatContainsASearchResult))(princ ", which containts at least one search result, is referenced in the block definition ")(princ (vla-get-Name blockDefinition))(princ "\n")
					(setq numberOfHits (+ 1 numberOfHits))
				)
			)
		)
	)
	(princ "\tfound ")(princ numberOfHits)(princ " references to ")(princ (vla-get-Name blockDefinitionThatContainsASearchResult))(princ "\n")
)
(princ)