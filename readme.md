2018/04/02

The package management system for ITG depends on having git and braid installed. 

To create a new ITG project, run makeNewProject.bat, passing a single parameter on the command line: the path of the new project folder that is to be created.

Sincerely,
Neil Jackson
neil@autoscaninc.command
425-218-6726 (cell)
206-282-1616 ext. 102 (office)

Autoscan, Inc.
4040 23RD AVE W
SEATTLE WA 98199-1209
206-282-1616


